class CraigslistScraper
	BASE_URL = "https://losangeles.craigslist.org/".freeze
	MODELS = ['honda+civic'].freeze

	def doit!
		MODELS.each do |car|
		  # no more than 100k miles AND not salvage AND max price of $11k
		  search_string = "search/cto?max_price=12000&auto_make_model=#{car}&max_auto_miles=100000&auto_title_status=1"
		  url = URI.encode(BASE_URL + search_string)
		  doc = Nokogiri::HTML(open(URI.encode(url)))

		  doc.css("li.result-row").each do |elem|
		    uuid  = elem.attributes['data-pid'].value.to_i
		    price = elem.children[3].children[7].children[1].children.text.gsub(/\D/, '').to_i
		    car_url = elem.children[1].attributes['href'].value

		    if car_url.include?('craigslist')
		      car_url = 'https:' + car_url
		    else
		      car_url = BASE_URL.chomp('/') + elem.children[1].attributes['href'].value
		    end

		    if Car.find_by(craigslist_id: uuid).nil? and price.is_a?(Integer) and price > 0
		      Car.create!(
		        craigslist_id: uuid,
		        name: elem.children[3].children[5].children.text,
		        url: car_url,
		        price: price.to_i,
		        make_and_model: car
		      )
		    end
		  end
		end
	end
end
