class CarMailer < ActionMailer::Base
  def fetch_cars cars
    @cars = cars
    return if @cars.empty?
    mail(to: 'erikbatres@gmail.com', from: 'info@carscraper.com', subject: 'Cars for Today')
  end
end
