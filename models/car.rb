class Car < ActiveRecord::Base
  validates_uniqueness_of :craigslist_id
  scope :today, -> { where("created_at >= ? AND created_at < ?", Date.today, Date.tomorrow) }
  scope :rav4,  -> { where(make_and_model: "toyota+rav4") }
  scope :prius, -> { where(make_and_model: "prius") }
  scope :honda_civic, -> { where(make_and_model: "honda+civic") }
end
