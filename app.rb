require 'sinatra'
require 'sinatra/config_file'
set :database_file, "./config/database.yml"
# in non modular sinatra apps db file must be set before requiring sinatra/activerecords
require 'sinatra/activerecord'
require 'open-uri'
require 'nokogiri'
require 'action_mailer'
require 'byebug'
require 'sinatra/config_file'
require './config/environments' #database configuration

config_file './config/config.yml'
require_relative './models/init.rb'
require_relative './services/init.rb'
require_relative './mailers/init.rb'
MODELS = ['honda+civic']

use Rack::Auth::Basic, "Restricted Area" do |username, password|
  username == 'batres' and password == 'pupusas'
end

configure do
  set :root,    File.dirname(__FILE__)
  set :views,   File.join(Sinatra::Application.root, 'views')

  ActionMailer::Base.smtp_settings = {
    :address => "smtp.gmail.com",
    :port => 587,
    :authentication => :login,
    :user_name => ENV['gmail_user_name'] || settings.gmail_user_name,
    :password =>  ENV['gmail_password']  || settings.gmail_password,
    :domain => "gmail.com",
  }
  ActionMailer::Base.view_paths = File.join(Sinatra::Application.root, 'views')
end

get '/' do
  CraigslistScraper.new.doit!
  "Car count on #{Time.now.to_date} is #{Car.count}"
end

get '/send' do
  CarMailer.fetch_cars(Car.today.honda_civic).deliver_now
  "success. check your email"
end
