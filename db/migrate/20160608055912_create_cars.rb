class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string    :name
      t.string    :url
      t.string    :craigslist_id
      t.string    :copy
      t.string    :make_and_model
      t.integer   :price

      t.timestamps
    end
  end
end
